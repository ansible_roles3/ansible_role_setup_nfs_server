Ansible_role_setup_nfs_server
=========

Роль позволяет равернуть и настроить серверную часть NFS.

Requirements
------------
```
collection:
  - community.general.ufw
```
You might already have this collection installed if you are using the ansible package. It is not included in ansible-core. To check whether it is installed, run ```ansible-galaxy collection list```.
To install it, use: ```ansible-galaxy collection install community.general```.

Role Variables
--------------

```
# vars file for ansible_role_setup_nfs_server
share_dir: "/mnt/share_directory" # Расшаренный NFS каталог
ip_nfs_client: "*" # IP адрес клиента NFS

```
По умолчанию разрешены подключения от любого NFS клиента.
Конфигурационный файл NFS сервера расположен в каталоге ```templates```.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
    - hosts: nfs-server
      roles:
         - ansible_role_setup_nfs_server
```

License
-------

BSD

Author Information
------------------
AlZa 2023
